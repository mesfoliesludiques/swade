declare interface Roll {
  options: any;
}

declare interface BaseEntityData {
  data: any;
}

declare interface Die {
  formula: any;
}

declare interface ItemSheetData {
  config: any;
}

declare interface ActorSheet {
  _createEditor(target: any, editorOptions: any, initialContent: any): any;
}

declare interface WebSocket {
  emit: any;
  on: any;
}
