/* eslint-disable no-unused-vars */
export enum TemplatePreset {
  CONE = 'cone',
  SBT = 'sbt',
  MBT = 'mbt',
  LBT = 'lbt',
}
